public class Card{
	private String suit;
	private String number;
	
	public Card(String suit, String number){
		this.suit = suit;
		this.number = number;
	}
	
	//getters
	public String getSuit(){
		return this.suit;
	}
	public String getNumber(){
		return this.number;
	}
	
	public double calculateScore(){
		double valueOfCard = 0;
		
		//Set interger value for the score based on the card number
		switch(this.number){
			case "ACE":
			valueOfCard += 1;
			break;
			case "JACK":
			valueOfCard += 11;
			break;
			case "QUEEN":
			valueOfCard += 12;
			break;
			case "KING":
			valueOfCard += 13;
			break;	
			default:
			valueOfCard = Double.valueOf(this.number);			
		}
		
		//Set decimal value for the score based on the card suit
		switch(this.suit){
			case "HEARTS":
			valueOfCard += 0.4;
			break;
			case "SPADES":
			valueOfCard += 0.3;
			break;
			case "DIAMONDS":
			valueOfCard += 0.2;
			break;
			case "CLUBS":
			valueOfCard += 0.1;
			break;
		}
		
		return valueOfCard;
	}
	
	//toString
	public String toString(){
		String output = "";
		
		//Text colours and background clours
		final String blackText = "\u001B[30m";
		final String whiteText = "\u001B[37m";	
		final String redText = "\u001B[31m";	
		final String blackBackground = "\u001B[40m";
		final String whiteBackground = "\u001B[47m";
		String cardColour = blackText;
		
		int cardHeight = 11; 

		String[] rows = new String[cardHeight];
		
		//Prints the card with the same colour as the backgound unless it's the middle where the card will be black or red depending on the suit
		for(int i = 0; i < cardHeight; i++){								
			if(this.suit.equals("HEARTS") || this.suit.equals("DIAMONDS"))
				cardColour = redText;
			else 
				cardColour = blackText;				
		
			if(i == cardHeight/2)
				rows[i] += " " + whiteBackground + whiteText + cardColour + " " + this.number + " of " + this.suit + " " + blackBackground;
			else
				rows[i] += " " + whiteBackground + whiteText + " " + this.number + " of " + this.suit  + " " + blackBackground;							
		}		
		
		for(int i = 0; i < cardHeight; i++){
			output += rows[i].substring(4) + "\n";
		}			
		
		return output;
	}
}