import java.util.Scanner;

public class SimpleWar{
	public static void main(String[] args){
		Scanner scan = new Scanner(System.in);
		
		System.out.println("\nHello player\n");
		
		System.out.println("Here are the rules : ");
		System.out.println("""
							***************************************************************************************************
								1. At the start of the game, there is a deck of 52 cards.                                           
								2. The deck is randomly shuffled.                          	                                        
								3. At each round, each of the two players draw one card from the deck (draw pile). 	                
								4. The two cards are compared and the player with the "higher card wins the round":               	
								    i. Hearts: beats all (most powerful)                         	                                
								   ii. Spades: beats all but heart (2nd most powerful)                         	                    
								  iii. Diamonds: only beats clubs (3nd most powerful)   						                    
								   iv. Clubs: weakest   						                                                    
								5. At the end of the round, the player with the higher card wins one point                          
								6. Steps 4 to 6 are repeated until the deck is empty.                                               
							***************************************************************************************************
						""");		
		
		System.out.println("[Press enter to begin]");		
		scan.nextLine();
		
		Deck deck = new Deck();
		deck.shuffle();
		
		int playerOnePoints = 0;
		int playerTwoPoints = 0;			
		
		int round = 1;
		
		//Text colours
		final String yellowText = "\u001B[33m";
		final String blueText = "\u001B[34m";
		final String whiteText = "\u001B[37m";	
		final String redText = "\u001B[31m";	
		final String greenText = "\u001B[32m";	
		
		while(deck.getNumberOfCards() > 0){
			System.out.println("This is round: " + round + "\n");		
			
			Card firstCard = deck.drawTopCard();
			Card secondCard = deck.drawTopCard();
			
			if(playerOnePoints > playerTwoPoints){
				System.out.println(yellowText + "Player one points: " + greenText + playerOnePoints);
				System.out.println(blueText + "Player two points: " + redText +playerTwoPoints + "\n");
			} else if (playerOnePoints < playerTwoPoints){
				System.out.println(yellowText + "Player one points: " + redText + playerOnePoints);
				System.out.println(blueText + "Player two points: " + greenText +playerTwoPoints + "\n");
			} else {
				System.out.println(yellowText + "Player one points: " + playerOnePoints);
				System.out.println(blueText + "Player two points: " + playerTwoPoints + "\n");
			}
				
			
			System.out.println(yellowText + "Player one card:\n" + firstCard);
			System.out.println(yellowText + "Player one card points: " + firstCard.calculateScore() + "\n");
			System.out.println(blueText + "Player two card:\n" + secondCard);
			System.out.println(blueText + "Player two card points: " + secondCard.calculateScore() + "\n");
			
			//Checks who has the higher card score
			if(firstCard.calculateScore() > secondCard.calculateScore()){
				System.out.println(yellowText + "!!!Player one wins!!!\n");
				playerOnePoints++;
			} else{
				System.out.println(blueText + "!!!Player two wins!!!\n");
				playerTwoPoints++;
			}
			
			if(playerOnePoints > playerTwoPoints){
				System.out.println(yellowText + "Player one points: " + greenText + playerOnePoints);
				System.out.println(blueText + "Player two points: " + redText +playerTwoPoints + whiteText + "\n");
			} else if (playerOnePoints < playerTwoPoints){
				System.out.println(yellowText + "Player one points: " + redText + playerOnePoints);
				System.out.println(blueText + "Player two points: " + greenText + playerTwoPoints + whiteText + "\n");
			} else {
				System.out.println(yellowText + "Player one points: " + playerOnePoints);
				System.out.println(blueText + "Player two points: " + playerTwoPoints + whiteText + "\n");
			}
			
			System.out.println("There are " + deck.getNumberOfCards() + " cards left in the deck\n");
			
			if(deck.getNumberOfCards() <= 1)
				System.out.println("[Press enter to end the game]");		
			else		
				System.out.println("[Press enter to start the next round]");		
			scan.nextLine();				
			
			System.out.println("**************************************\n");
			
			round++;
		}
		
		if(playerOnePoints == playerTwoPoints)
			System.out.println("It's a draw!!");
		else if(playerOnePoints > playerTwoPoints)
			System.out.println(yellowText + "Congratulations player one!!");
		else
			System.out.println(blueText + "Congratulations player two!!");
	}
}